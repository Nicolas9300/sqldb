== Informe Base de Datos I  (COM-01)
Gastón Nicolás Campos <gaston.campos509@gmail.com>; Julieta Belén Rocha<julietamd45@gmail.com>; Matías Ariel Balbi <matuteariel96@gmail.com>; Nicolás Reinaldo <nicolas01@hotmail.com.ar>
v1, {docdate}. Docentes Czmerinski Hernán Rondelli Hernán Daniel y Argañaráz Omar Javier (COM-01)

:title-page:
:numbered:
:source-highlighter: coderay
:tabsize: 4

== _Introducción_
El trabajo práctico consistía en la creación de una base de datos
escrita en SQL donde almacenamos información sobre tarjetas de crédito.
Dentro de dicha base de datos, creamos las tablas correspondientes con
sus pk's y fk's, se cargaron 20 datos de clientes junto con sus
respectivas tarjetas de crédito y 20 comercios donde pudieron haber
realizado alguna compra.
Dentro del código se incluyen las tres funciones pedidas:

- *Autorización de compra*.

- *Generación del resumen*.

- *Alertas a clientes*.

El código realizado en SQL se puede escribir y ejecutar en una
aplicación CLI escrita en Go. Además, para poder comparar el modelo
relacional NoSQL, se guardan los datos cargados de comercios,clientes,
tarjetas y compras realizadas,en una base de datos NoSQL basada en JSON.
Para realizar esto, utilizamos la base de datos BoltDB que a su vez
también se ejecuta desde una aplicación CLI escrita en Go.

== _Descripción_
Para comenzar, primero nos repartimos las tablas que cada integrante
debía cargar a la base de datos con sus respectivas pk´s y fk´s.
Al principio surgieron problemas básicos con el uso de git y el manejo
del teclado de la máquina virtual, pero siempre como grupo nos
ayudabamos para solucionar cada problema lo antes posible y no
atrasarnos en el trabajo. Luego, nos fuimos asignando diferentes tareas
de acuerdo a lo que cada uno quería implementar. Esto se charlaba en
grupo y se llegaba a un acuerdo para no generar inconvenientes y para que no se realicen dos veces
las mismas cosas.

En general la gran mayoría de problemas que teníamos usualmente en el trabajo era a la hora de realizar casteos de datos o extracciones, por ejemplo, `extract(year from fecha)`
que precisamente lo que realiza es obtener el año de una fecha y eso extraerlo a tipo int para que podamos trabajar con el dato en cuestión. Otro ejemplo de esto se puede ver en `to_date(tarjeta.validahasta, 'YYYYMM')` en el cual lo que se estaría realizando es un casteo a tipo _Date_ con el dato proporcionado, que en la gran mayoría de casos el dato que se le pasa por parámetro es un char similar a “202206”, con “YYYYMM” indicaremos que queremos la fecha por año-mes y nos quedaría como resultado “2022-06”. Tambien a la hora de hacer las alertas tuvimos dificultades en pensar la 
manera de sacar los minutos para poder corroborar si era menor a 5 min o 1 min. Finalmente investigamos y descubrimos el `greatest()` que calcula el valor absoluto de dos numeros. Con esto pudimos facilmente obtener los minutos.

== _Implementación_

Si bien realizamos la creación de tablas y tambien inserción de datos dentro de las mismas en esta sección vamos a profundizar más en cuanto a las funcionalidades pedidas, las cuales son las siguientes:

1. *Autorización de compra*.

Dicha funcionalidad fue implementada en tres stored procedures y un trigger, como se puede ver a continuación.

La funcion _obtener_limite_compra_ como tal recibe por parámetro un número de tarjeta y devuelve el monto total de la tarjeta después de que se realizarán compras bajo ese número, por ejemplo, si una tarjeta tiene asociada tres compras entonces el límite va a ser la suma de esas tres compras descontando el límite de la tarjeta en ese momento.
 
.Función para saber el limite de compra de una tarjeta
[source, sql]
----
create or replace function obtener_limite_compra(nro_tarjeta char(16)) returns decimal as $$
declare
	monto_total decimal := 0;
	monto_compra decimal;
begin
	
    for monto_compra in select monto from compra where nrotarjeta = nro_tarjeta loop
        monto_total := monto_total + monto_compra;
    end loop;

	return monto_total;

end;
$$ language plpgsql;

----

La función "*funcion_valida*" recibe como parámetro número de tarjeta, código de seguridad, número de comercio y monto total de la compra, y evalúa:
 
- Si la tarjeta se encuentra ‘_Vigente_’.
- Que el código de seguridad sea correcto.
- Que el límite de compra de la tarjeta no se haya sobrepasado, por ende, que la compra no se pueda aplicar por falta de fondos.
- Y por último que la tarjeta no se encuentre vencida.
 
En caso de que se cumpla con algún criterio anteriormente nombrado se va a generar un "_rechazo_" indicando el motivo por el cual no se pudo realizar la compra satisfactoriamente, y la función va a retornar "_false_" indicando que dicha compra no se pudo realizar.

.Función para validar la tarjeta
[source, sql]
----
create or replace function funcion_valida(nro_tarjeta char(16), cod_seguridad char(4), nro_comercio int, precio decimal(7,2)) returns boolean as $$
declare
    tarjeta record;
    monto_total decimal;
    tarjetavalida boolean := true;
begin

    select * into tarjeta from tarjeta t where t.nrotarjeta=nro_tarjeta;

    monto_total := obtener_limite_compra(nro_tarjeta) + precio;

	if tarjeta is null then
		tarjetavalida := false;
    elsif tarjeta.estado = 'anulada' then
        insert into rechazo (nrotarjeta,nrocomercio,fecha,monto,motivo) values (nro_tarjeta, nro_comercio, current_date, precio, 'Tarjeta no valida');
        tarjetavalida := false;
    elsif tarjeta.codseguridad != cod_seguridad then
        insert into rechazo (nrotarjeta,nrocomercio,fecha,monto,motivo) values (nro_tarjeta, nro_comercio, current_date, precio, 'Codigo de seguridad no valido');
        tarjetavalida := false;
    elsif tarjeta.limitecompra < monto_total then
        insert into rechazo (nrotarjeta,nrocomercio,fecha,monto,motivo) values (nro_tarjeta, nro_comercio, current_date, precio, 'Supera limite de tarjeta');
        tarjetavalida := false;
    elsif to_date(tarjeta.validahasta, 'YYYYMM') < current_date then
        insert into rechazo (nrotarjeta,nrocomercio,fecha,monto,motivo) values (nro_tarjeta, nro_comercio, current_date, precio, 'Plazo de vigencia expirado');
        tarjetavalida := false;
    elsif tarjeta.estado = 'suspendida' then
        insert into rechazo (nrotarjeta,nrocomercio,fecha,monto,motivo) values (nro_tarjeta, nro_comercio, current_date, precio, 'La tarjeta se encuentra suspendida');
        tarjetavalida := false;
    end if;
return tarjetavalida;
end;
$$ language plpgsql;
----

La función “_funcion_autorizar_” va a llamar a la función anteriormente nombrada con los datos que se pasan a la hora de hacer el insert a la tabla “_consumo_”, y va a realizar la compra en caso de que todo esté correcto 

.Función para realizar la compra
[source, sql]
----
create or replace function funcion_autorizar() returns trigger as $$
declare
    valida boolean;
begin

    select funcion_valida(new.nrotarjeta, new.codseguridad, new.nrocomercio, new.monto) into valida;

    if valida then
        insert into compra (nrotarjeta,nrocomercio,fecha,monto,pagado) values (new.nrotarjeta,new.nrocomercio,current_date,new.monto,false);
    end if;

    return new;
end;
$$ language plpgsql;
----

Por último, en el siguiente trigger que se muestra a continuación se va a llamar a la función anteriormente explicada antes de que suceda un "_insert_" dentro de la tabla “_consumo_”, y con esto verificar si es válida para proceder a una compra o si es declarado como un rechazo. 

.Trigger para detectar una inserción 
[source, sql]
----
create trigger autorizar_trigger
before insert on consumo
for each row
execute procedure funcion_autorizar();
----
[start=2]
1. *Generación del resumen*.

La lógica de esta función fue implementada haciendo uso de 3 stored procedures, a continuación se muestra una breve explicación de las funciones.

Antes de comenzar con el resumen cargamos los cierres con la función "*cargar_cierres*", esta funcion no recibe parametros y su proposito
cargar la tabla cierre segun la cantidad de meses y las terminaciones de las tarjetas, modificando el periodo de inicio, cierre y 
vencimiento de la tarjeta asi una vez cargada la tabla se avanza otro día. 

.Función para cargar los cierres
[source, sql]
----
create or replace function cargar_cierres() returns void as $$
declare
	fechainicio date;
	fechacierre date;
	fechavto date;
begin

	for i in 1 ..12 loop
		fechainicio := '2021-'||i||'-01';	
		fechacierre := '2021-'||i||'-25';
		fechavto := '2021-'||i||'-28';

		for j in 0 ..9 loop
			
			insert into cierre values (2021,i,j, fechainicio, fechacierre, fechavto);

			fechainicio := fechainicio + interval '1 day';  --Se le suma 1 dia a cada fecha
			fechacierre := fechacierre + interval '1 day';
			fechavto := fechavto + interval '1 day';

		end loop;

	end loop;
	
end;
$$ language plpgsql;

----
[start=3]
1. *Detección de alertas*


Esta función recibe por parámetros un rechazo que se haya ingresado recientemente a la tabla _rechazo_ y compara si existe dos rechazos que se hayan realizado en el mismo día, y que el motivo de rechazo haya sido porque se superaba el límite de la tarjeta, si esto último sucede entonces se lanza una alerta indicando que se trató de comprar dos veces en el mismo día y que no se pudo por el límite de compra.Cuando se genera esta alerta,la tarjeta en cuestión queda suspendida por tiempo indefinido.

.Funcion alerta_limite_compra
[source, sql]
----
create or replace function alerta_limite_compra(nro_rechazo int,nro_tarjeta char(16), fecha timestamp, motivo text) returns boolean as $$
declare
    r record; --rechazo
    alerta_limite boolean := false;

    añorechazo1 int := extract(year from fecha);
    mesrechazo1 int := extract(month from fecha);
    diarechazo1 int := extract(day from fecha);

    añorechazo2 int;
    mesrechazo2 int;
    diarechazo2 int;

    motivo_rechazo text:='Supera limite de tarjeta';

begin

    for r in select * from rechazo where nrotarjeta = nro_tarjeta loop

            añorechazo2 := extract(year from r.fecha);
            mesrechazo2 := extract(month from r.fecha);
            diarechazo2 := extract(day from r.fecha);

            --Se verifica que no este en el rechazo y que sea del mismo motivo
            if r.nrorechazo!=nro_rechazo and añorechazo1=añorechazo2 and mesrechazo1=mesrechazo2 and diarechazo1=diarechazo2 and motivo=motivo_rechazo and r.motivo=motivo_rechazo then

                insert into alerta (nrotarjeta,fecha,nrorechazo,codalerta,descripcion) values (nro_tarjeta,fecha,nro_rechazo,32,motivo);
                update tarjeta set estado = 'suspendida' where nrotarjeta = nro_tarjeta;
                alerta_limite := true;

            end if;
    end loop;

    return alerta_limite;
end;
$$ language plpgsql;
----

La función _verificar_rechazo_ lo que hace en resumidas cuenta es primero enviar los datos del rechazo, que se acaba de insertar en la tabla _rechazo_, hacia la función explicada recientemente arriba y que verifique si no existe con anterioridad un rechazo que se haya dado en el mismo dia y que haya sido por el mismo motivo (Falta de crédito en la tarjeta), por último, se inserta el rechazo generado como un alerta dentro de la base de datos.

.Funcion verificar_rechazo
[source, sql]
----
create or replace function verificar_rechazo(nro_rechazo int, nro_tarjeta char(16), nro_comercio int, fecha timestamp, motivo text) returns void as $$
begin

    perform alerta_limite_compra(nro_rechazo,nro_tarjeta, fecha, motivo);

    --Si no es la alerta de limite de compra se toma como una alerta normal
    insert into alerta (nrotarjeta,fecha,nrorechazo,codalerta,descripcion) values (nro_tarjeta,fecha,nro_rechazo,0,motivo);

end;
$$ language plpgsql;
----

.Funcion_alerta_rechazos
[source, sql]
----
create or replace function funcion_alerta_rechazos() returns trigger as $$
begin
    perform verificar_rechazo(new.nrorechazo, new.nrotarjeta, new.nrocomercio, new.fecha, new.motivo);

    return new;
end;
$$ language plpgsql;
----
Esta funcion recibe los minutos como parametro para despues implementarlo con los if. Cada vez que se hace una
compra se activa el trigger y ejecuta esta funcion, en donde compara la compra echa con las otras que la persona 
hizo anteriormente. Si se encuentra una compra parecida en los tiempos indicados para que sea una alerta se agrega
en la tabla "alerta" con su respectivo caso. Si fue en un tiempo menor a 1 o 5 min y respetando las consignas de cada
caso.

.Funcion alerta_compra_minutos
[source, sql]
----
create or replace function alerta_compra_minutos(codalerta int) returns void as $$
declare
	c1 record; --Compra 1
	c2 record; --Compra 2

	codpostalcompra1 char(8);
	codpostalcompra2 char(8);

	diferencia_tiempo interval;
begin

	for c1 in select * from compra loop --Recorre las compras

		select codigopostal into codpostalcompra1 from comercio where nrocomercio = c1.nrocomercio;

		for c2 in select * from compra where nrotarjeta =c1.nrotarjeta loop --Se recorren todas las compras con esa misma tarjeta

			select codigopostal into codpostalcompra2 from comercio where nrocomercio = c2.nrocomercio;

			diferencia_tiempo := greatest((c1.fecha-c2.fecha),(c2.fecha-c1.fecha)); --Con esto se calcula el valor absoluto de la fecha

			if codalerta=1  and diferencia_tiempo <= '00:01:00'::interval and c1.nrocomercio!=c2.nrocomercio and codpostalcompra1=codpostalcompra2 and alerta_duplicada(c1.nrotarjeta,codalerta)=false then

				insert into alerta (nrotarjeta,fecha,nrorechazo,codalerta,descripcion) values (c1.nrotarjeta,current_timestamp,null,codalerta,'Se realizaron dos compras en menos de un minuto');


			elsif codalerta=5 and diferencia_tiempo <= '00:05:00'::interval and c1.nrocomercio!=c2.nrocomercio  and codpostalcompra1!=codpostalcompra2 and alerta_duplicada(c1.nrotarjeta,codalerta)=false then

				insert into alerta (nrotarjeta,fecha,nrorechazo,codalerta,descripcion) values (c1.nrotarjeta,current_timestamp,null,codalerta,'Se realizaron dos compras en diferentes comercios en menos de cinco minutos');
				

			end if;

		end loop;

	end loop;

end;
$$ language plpgsql;
----

La funcion _alerta_duplicada_ simplemente recibe por parámetros un número de tarjeta y un código de alerta y verifica si no hubo una alerta vinculada con la tarjeta proporcionada en donde el código de alerta sea el mismo que se le pasa por parámetro. 

.Funcion alerta_duplicada
[source, sql]
----
create or replace function alerta_duplicada(nro_tarjeta char(16), cod_alerta int) returns boolean as $$
declare
	alerta_encontrada record;
begin
	select * into alerta_encontrada from alerta where nrotarjeta=nro_tarjeta and codalerta=cod_alerta;
	
	if not found then
		return false;
	end if;

	return true;
end;
$$ language plpgsql;
----
.Funcion_alerta_compras_min()
[source, sql]
----
create or replace function funcion_alerta_compras_min() returns trigger as $$
begin
	perform alerta_compra_minutos(1); --Verifica las compras con un tiempo menor a 1min

	perform alerta_compra_minutos(5); --Verifica las compras con un tiempo menor a 5min

	return new;
end;
$$ language plpgsql;
----
.Triggers para crear las alertas
[source, sql]
----
create trigger comprobar_rechazos
after insert on rechazo
for each row
execute procedure funcion_alerta_rechazos();

create trigger alertas_compras
after insert on compra
for each row
execute procedure funcion_alerta_compras_min();
----

== _Conclusiones_

Para concluir este trabajo, podemos decir que trabajando en equipo
logramos cumplir con el objetivo que se nos propuso al principio,
pudiendo resolver cada inconveniente que surgió investigando o
ayudandonos en grupo.También aplicamos los conceptos aprendidos en
cada clase.
Finalmente, aprendimos como conectar con una base de datos (Postgres y
BoltDB) y ejecutar diferentes funciones desde la creación de un menú
para poder manejar la aplicación, creada en Go.
