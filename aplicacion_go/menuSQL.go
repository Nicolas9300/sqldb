package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
)

func crearDataBase() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec("drop database if exists tarjetas")
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec("create database tarjetas")
	if err != nil {
		log.Fatal(err)
	}
}

func crearTablas() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjetas sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(`
create table cliente(nrocliente	int,nombre text,apellido text,domicilio	text,telefono char(12));
create table tarjeta(nrotarjeta	char(16),nrocliente int,validadesde char(6),validahasta	char(6),codseguridad char(4),limitecompra decimal(8,2),estado char(10));
create table comercio(nrocomercio int,nombre text,domicilio text,codigopostal char(8),telefono char(12));
create table compra(nrooperacion serial,nrotarjeta char(16),nrocomercio int,fecha timestamp,monto decimal(7,2),pagado boolean);
create table rechazo(nrorechazo	serial,nrotarjeta char(16),nrocomercio int,fecha timestamp,monto decimal(7,2),motivo text);
create table detalle(nroresumen	int,nrolinea int,fecha date,nombrecomercio text,monto decimal(7,2));
create table alerta(nroalerta serial,nrotarjeta char(16),fecha timestamp,nrorechazo int,codalerta int,descripcion text);
create table cierre(ano int,mes int,terminacion int,fechainicio date,fechacierre date,fechavto date);
create table cabecera(nroresumen serial,nombre text,apellido text,domicilio text,nrotarjeta char(16),desde date,hasta date,vence date,total decimal(8,2));
create table consumo(nrotarjeta	char(16),codseguridad char(4),nrocomercio int,monto decimal(7,2));`)
	if err != nil {
		log.Fatal(err)
	}

}

func asignarPKyFK() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjetas sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(`
alter table cliente add constraint cliente_pk primary key (nrocliente);
alter table tarjeta add constraint tarjeta_pk primary key (nrotarjeta);
alter table tarjeta add constraint tarjeta_nrocliente foreign key (nrocliente)references cliente(nrocliente);
alter table comercio add constraint comercio_pk primary key (nrocomercio);
alter table compra add constraint compra_pk primary key (nrooperacion);
alter table compra add constraint compra_nrotarjeta foreign key (nrotarjeta) references tarjeta(nrotarjeta);
alter table compra add constraint compra_nrocomercio foreign key (nrocomercio) references comercio(nrocomercio);
alter table rechazo add constraint rechazo_pk primary key (nrorechazo);
alter table rechazo add constraint rechazo_nrotarjeta foreign key (nrotarjeta) references tarjeta(nrotarjeta);
alter table rechazo add constraint rechazo_nrocomercio foreign key (nrocomercio) references comercio(nrocomercio);
alter table cabecera add constraint cabecera_pk primary key (nroresumen);
alter table cabecera add constraint cabecera_nrotarjeta foreign key (nrotarjeta) references tarjeta(nrotarjeta);
alter table detalle add constraint detalle_pk primary key (nroresumen, nrolinea);
alter table detalle add constraint detalle_nroresumen foreign key (nroresumen) references cabecera(nroresumen);
alter table alerta add constraint alerta_pk primary key (nroalerta);
alter table alerta add constraint alerta_nrotarjeta foreign key (nrotarjeta) references tarjeta(nrotarjeta);
alter table alerta add constraint alerta_nrorechazo foreign key (nrorechazo) references rechazo(nrorechazo);
alter table cierre add constraint cierre_pk primary key (ano, mes, terminacion);`)
	if err != nil {
		log.Fatal(err)
	}

}

func cargarTablas() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjetas sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(`
insert into cliente values(1,'Ricardo','Rojas','Paso de los Andes 2194',1125789715);
insert into cliente values(2,'Rocio','Paredes','Soler 2975',1157946828);
insert into cliente values(3,'Luciano','Monzon','El Salvador 5794',1168756849);
insert into cliente values(4,'Roberto','Palavecino','Los Lobos 4268',1168759526);
insert into cliente values(5,'Lucia','Velázques','Alondra 1689',1186163895);
insert into cliente values(6,'Gonzalo','Martínez','Juan María Gutiérrez 1150',1156798831);
insert into cliente values(7,'Marcelo','Gallardo','Uriburu 528',1175642394);
insert into cliente values(8,'Francisco','Galván','Mendoza 4785',1198962745);
insert into cliente values(9,'Mateo','Almirón','Alvear 1212',1146329655);
insert into cliente values(10,'Lionel','Rodríguez','Manuel Castro 3335', 1160713452);
insert into cliente values(11,'Julian','Alvarez','Pasco 1321','1118120909');
insert into cliente values(12,'Zunilda','Fernandez','Paso de Uspallata 840','1144229013');
insert into cliente values(13,'Matias','Bauman','Palpa 565','1109091218');
insert into cliente values(14,'Jorge','Carrazcal','Uruguay 44','1140778307');
insert into cliente values(15,'Rosa','Ramirez','Beaucheff 3041','1134899000');
insert into cliente values(16,'Thomas','Bussolari','Cangallo 607','1123908775');
insert into cliente values(17,'Leonardo','Ponzio','Mozart 22','1166088505');
insert into cliente values(18,'Claudia','Falcone','Alfredo Palacios 902','1177345509');
insert into cliente values(19,'Enzo','Perez','Jose Hernandez 5272','1159990402');
insert into cliente values(20,'Elvira','Toranzo','Almirante Brown 2038','1124786633');
insert into tarjeta values(5498785897456512,1,202011,202110,4889,50000,'vigente');
insert into tarjeta values(5686885897586512,1,201811,202212,4989,30000,'vigente');
insert into tarjeta values(5438775897453512,2,201611,202204,2959,55000,'vigente');
insert into tarjeta values(5456885856456565,2,201911,202812,6798,60000,'vigente'); 
insert into tarjeta values(2598785897456512,3,201611,202512,6872,100000,'vigente');
insert into tarjeta values(7598785897456512,4,201711,202312,9720,20000,'vigente');
insert into tarjeta values(8298785897456512,5,201911,202112,5976,150000,'vigente');
insert into tarjeta values(5498768897456512,6,201211,202310,5068,60000,'vigente');
insert into tarjeta values(5498785977456512,7,202011,202705,0679,55000,'vigente');
insert into tarjeta values(4713873498793139,8,201911,202307,0016,75000,'vigente');
insert into tarjeta values(6482167412032846,9,201605,202501,4678,150000,'suspendida');
insert into tarjeta values(6491219721373208,10,201701,202303,3972,350000,'vigente');
insert into tarjeta values(6491219751654813,11,201705,202304,0068,300000,'vigente');
insert into tarjeta values(4579516796518431,12,201707,202503,3591,390000,'vigente');
insert into tarjeta values(6491543241373208,13,201801,202610,6782,970000,'vigente');
insert into tarjeta values(6143219721373208,14,202002,202811,8972,160000,'vigente');
insert into tarjeta values(3429121975173208,15,201706,203003,9812,500000,'vigente');
insert into tarjeta values(6132465687153208,16,201601,202212,0128,680000,'vigente');
insert into tarjeta values(1231984838975686,17,201510,202303,1687,970000,'anulada');
insert into tarjeta values(5497465189413096,18,201807,202405,5976,100000,'vigente');
insert into tarjeta values(9845462064816876,19,201708,202507,5198,168000,'vigente');
insert into tarjeta values(1016484616786189,20,201709,202408,6873,500000,'vigente');
insert into comercio values(1,'La central','Avenida Peron 5498','B2701XAA','02320-486879');
insert into comercio values(2,'Carrefour','Remedios de Escalada 2234','B2701XAA','02320-484913');
insert into comercio values(3,'Vital','Jose de San Martin 666','B2701XAD','02320-487777');
insert into comercio values(4,'Easy','Avenida Presidente Arturo Illia 8066','B2701XAE','02320-487791');
insert into comercio values(5,'Sodimac','Avenida Olivos 555','B2701XAF','02320-487755');
insert into comercio values(6,'Mostaza','Avenida Eva Peron 424','B6501XAC','02320-486666');
insert into comercio values(7,'Jumbo','El Callao 6692','B6501XAA','02320-487244');
insert into comercio values(8,'Zara','Ramal Pilar 9998','B6501XAB','02320-484444');
insert into comercio values(9,'Calzados salvador','Batalla de Chacabuco 506','B6501XAD','02320-488826');
insert into comercio values(10,'Garbarino','Paso de los Andes 274','B6501XAE','02320-489899');
insert into comercio values(11,'Starbucks','Paso de los Patos 244','B7313XAA','02320-485522');
insert into comercio values(12,'Fravega','Batalla de Maipu 2345','B7313XAB','02320-483333');
insert into comercio values(13,'Burger king','Santos Vega 44','B7313XAC','02320-481113');
insert into comercio values(14,'Vea','Cabo Sosa 241','B7313XAF','02320-482828');
insert into comercio values(15,'Disco','Nazca 4568','B7313XAG','02320-489766');
insert into comercio values(16,'Freddo','Cuyo 330','B2705XAB','02320-487411');
insert into comercio values(17,'Musimundo','Artigas 5352','B2707XAC','02320-480011');
insert into comercio values(18,'Libreria Mundo Nuevo','Nicolas Mascardi 3998','B2707XAD','02320-486565');
insert into comercio values(19,'Vete al diablo','Obrien 274','B2707XAE','02320-482215');
insert into comercio values(20,'Coto','Avenida Cabildo 25','B7313XAE','02320-481213');`)
	if err != nil {
		log.Fatal(err)
	}

}

func crearStoredYTriggers() {

	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjetas sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(`
create or replace function borrarPKsyFKs() returns void as $$
begin
	alter table tarjeta drop constraint tarjeta_nrocliente;
	alter table compra drop constraint compra_nrotarjeta;
	alter table compra drop constraint compra_nrocomercio;
	alter table rechazo drop constraint rechazo_nrotarjeta;
	alter table rechazo drop constraint rechazo_nrocomercio;
	alter table cabecera drop constraint cabecera_nrotarjeta;
	alter table detalle drop constraint detalle_nroresumen;
	alter table alerta drop constraint alerta_nrotarjeta;
	alter table alerta drop constraint alerta_nrorechazo;

	alter table cliente drop constraint cliente_pk;
    	alter table tarjeta drop constraint tarjeta_pk;
	alter table comercio drop constraint comercio_pk;
    	alter table compra drop constraint compra_pk;
	alter table rechazo drop constraint rechazo_pk;
	alter table cabecera drop constraint cabecera_pk;
	alter table detalle drop constraint detalle_pk;
	alter table alerta drop constraint alerta_pk;
	alter table cierre drop constraint cierre_pk;
end;
$$ language plpgsql;

create or replace function cargar_consumos() returns void as $$
begin
	insert into consumo values (5498785897456512, 4889, 1, 500);   --Rechazo tarjeta vencida
	insert into consumo values (5686885897586512, 4989, 2, 1500);  --Compra
	insert into consumo values (5686885897586512, 4989, 13, 2000); --Compra
	insert into consumo values (5498785897456512, 4877, 1, 500);   --Rechazo cod de seguridad invalido
	insert into consumo values (2598785897456512, 6872, 15, 4000); --Compra
	insert into consumo values (5686885897586512, 4989, 2, 50000); --Rechazo monto total de tarjeta superado
	insert into consumo values (6482167412032846, 4678, 9, 2000);  --Rechazo tarjeta suspendida
	insert into consumo values (1016484616786189, 6873, 10, 3500); --Compra
	insert into consumo values (6143219721373208, 8972, 12, 30500);--Compra
	insert into consumo values (1231984838975686, 1687, 13, 1000); --Rechazo tarjeta anulada
	insert into consumo values (2598785897456512, 6872, 19, 50000);--Compra
	insert into consumo values (6491219751654813, 0068, 4, 15000); --Compra
	insert into consumo values (7598785897456512, 9720, 14, 5489); --Compra
	insert into consumo values (5686885897586512, 4989, 4, 25000); --Compra
	insert into consumo values (7598785897456512, 9720, 4, 15000); --Rechazo por limite de compra
	insert into consumo values (7598785897456512, 9720, 4, 15000); --Rechazo por limite de compra con la misma tarjeta
	insert into consumo values (4579516796518431, 3591, 1, 500);   --Compra
	insert into consumo values (4579516796518431, 3591, 2, 3000);  --Compra / alerta por compra en menos de 1 
end;
$$ language plpgsql;

create or replace function obtener_limite_compra(nro_tarjeta char(16)) returns decimal as $$
declare
	monto_total decimal := 0;
	monto_compra decimal;
begin
	
    for monto_compra in select monto from compra where nrotarjeta = nro_tarjeta loop
        monto_total := monto_total + monto_compra;
    end loop;

	return monto_total;
end;
$$ language plpgsql;

create or replace function funcion_valida(nro_tarjeta char(16), cod_seguridad char(4), nro_comercio int, precio decimal(7,2)) returns boolean as $$
declare
    tarjeta record;
    monto_total decimal;
    tarjetavalida boolean := true;
begin

    	select * into tarjeta from tarjeta t where t.nrotarjeta=nro_tarjeta;

	monto_total := obtener_limite_compra(nro_tarjeta) + precio;

	if tarjeta is null or tarjeta.estado = 'anulada' then
		tarjetavalida := false;
        insert into rechazo (nrotarjeta,nrocomercio,fecha,monto,motivo) values (nro_tarjeta, nro_comercio, current_date, precio, 'Tarjeta no valida');
    elsif tarjeta.codseguridad != cod_seguridad then
        insert into rechazo (nrotarjeta,nrocomercio,fecha,monto,motivo) values (nro_tarjeta, nro_comercio, current_date, precio, 'Codigo de seguridad no valido');
        tarjetavalida := false;
    elsif tarjeta.limitecompra < monto_total then
        insert into rechazo (nrotarjeta,nrocomercio,fecha,monto,motivo) values (nro_tarjeta, nro_comercio, current_date, precio, 'Supera limite de tarjeta');
        tarjetavalida := false;
    elsif to_date(tarjeta.validahasta, 'YYYYMM') < current_date then
        insert into rechazo (nrotarjeta,nrocomercio,fecha,monto,motivo) values (nro_tarjeta, nro_comercio, current_date, precio, 'Plazo de vigencia expirado');
        tarjetavalida := false;
    elsif tarjeta.estado = 'suspendida' then
        insert into rechazo (nrotarjeta,nrocomercio,fecha,monto,motivo) values (nro_tarjeta, nro_comercio, current_date, precio, 'La tarjeta se encuentra suspendida');
        tarjetavalida := false;
    end if;
return tarjetavalida;
end;
$$ language plpgsql;

create or replace function funcion_autorizar() returns trigger as $$
declare
    valida boolean;
begin

    select funcion_valida(new.nrotarjeta, new.codseguridad, new.nrocomercio, new.monto) into valida;

    if valida then
        insert into compra (nrotarjeta,nrocomercio,fecha,monto,pagado) values (new.nrotarjeta,new.nrocomercio,current_date,new.monto,false);
    end if;

    return new;
end;
$$ language plpgsql;

create or replace function cargar_cierres() returns void as $$
declare
	fechainicio date;
	fechacierre date;
	fechavto date;
begin

	for i in 1 ..12 loop
		fechainicio := '2021-'||i||'-01';	
		fechacierre := '2021-'||i||'-25';
		fechavto := '2021-'||i||'-28';

		for j in 0 ..9 loop
			
			insert into cierre values (2021,i,j, fechainicio, fechacierre, fechavto);

			fechainicio := fechainicio + interval '1 day';
			fechacierre := fechacierre + interval '1 day';
			fechavto := fechavto + interval '1 day';

		end loop;

	end loop;
	
end;
$$ language plpgsql;

create or replace function funcion_resumen(nro_cliente int,mes_ int) returns void as $$
declare
	t record;  --tarjeta
	cliente_ record;
    	nro_linea int;
    	nombre_comercio text;
	com record;  --compra
	c record;   --cierre
	total_ decimal(8,2);
	desde_ date;
	hasta_ date;
	vto_ date;
	
begin
	for t in select * from tarjeta where nrocliente=nro_cliente loop 
		total_ := 0;
		
		select * into c from cierre where mes=mes_ and terminacion = substr(t.nrotarjeta, length(t.nrotarjeta))::int;
		desde_ := c.fechainicio;
		hasta_ := c.fechacierre;
		vto_ := c.fechavto;
	
		for com in select * from compra where nrotarjeta=t.nrotarjeta loop  
			if extract(month from com.fecha) = mes_ then  
				total_ := total_ + com.monto; 				
			end if;
	
		end loop;
	
		select * into cliente_ from cliente where nrocliente=nro_cliente; 
		insert into cabecera(nombre,apellido,domicilio,nrotarjeta,desde,hasta,vence,total) values (cliente_.nombre,cliente_.apellido,cliente_.domicilio,t.nrotarjeta,desde_,hasta_,vto_,total_);

		perform cargar_detalles(t.nrotarjeta,mes_);
	
	end loop;
	
end;
$$ language plpgsql;

create or replace function cargar_detalles(nro_tarjeta char(16), mes_ int) returns void as $$
declare
	nro_resumen int; 
    	nro_linea int := 1; 
	com record;  --compra
	nombre_comercio text;
begin

	select nroresumen into nro_resumen from cabecera where nrotarjeta = nro_tarjeta;
	
	for com in select * from compra where nrotarjeta = nro_tarjeta loop  
            if extract(month from com.fecha) = mes_ then   

				select nombre into nombre_comercio from comercio where com.nrocomercio = nrocomercio;

                		insert into detalle values (nro_resumen, nro_linea, com.fecha, nombre_comercio, com.monto);

				update compra set pagado=true where nrooperacion = com.nrooperacion;
				
				nro_linea := nro_linea + 1;

			end if;
	end loop;

end;
$$ language plpgsql;

create or replace function alerta_limite_compra(nro_rechazo int,nro_tarjeta char(16), fecha timestamp, motivo text) returns boolean as $$
declare
	r record; --rechazo
	alerta_limite boolean := false;

	añorechazo1 int := extract(year from fecha);
	mesrechazo1 int := extract(month from fecha);
	diarechazo1 int := extract(day from fecha);

	añorechazo2 int;
	mesrechazo2 int;
	diarechazo2 int;

	motivo_rechazo text:='Supera limite de tarjeta';

begin

	for r in select * from rechazo where nrotarjeta = nro_tarjeta loop

			añorechazo2 := extract(year from r.fecha);
			mesrechazo2 := extract(month from r.fecha);
			diarechazo2 := extract(day from r.fecha);

			if r.nrorechazo!=nro_rechazo and añorechazo1=añorechazo2 and mesrechazo1=mesrechazo2 and diarechazo1=diarechazo2 and motivo=motivo_rechazo and r.motivo=motivo_rechazo then

				insert into alerta (nrotarjeta,fecha,nrorechazo,codalerta,descripcion) values (nro_tarjeta,fecha,nro_rechazo,32,motivo);
				update tarjeta set estado = 'suspendida' where nrotarjeta = nro_tarjeta;
				alerta_limite := true;

			end if;
	end loop;

	return alerta_limite;
end;
$$ language plpgsql;

create or replace function alerta_compra_minutos(codalerta int) returns void as $$
declare
	c1 record; --Compra 1
	c2 record; --Compra 2

	codpostalcompra1 char(8);
	codpostalcompra2 char(8);

	diferencia_tiempo interval;
begin

	for c1 in select * from compra loop

		select codigopostal into codpostalcompra1 from comercio where nrocomercio = c1.nrocomercio;

		for c2 in select * from compra where nrotarjeta =c1.nrotarjeta loop 

			select codigopostal into codpostalcompra2 from comercio where nrocomercio = c2.nrocomercio;

			diferencia_tiempo := greatest((c1.fecha-c2.fecha),(c2.fecha-c1.fecha)); 

			if codalerta=1  and diferencia_tiempo <= '00:01:00'::interval and c1.nrocomercio!=c2.nrocomercio and codpostalcompra1=codpostalcompra2 and alerta_duplicada(c1.nrotarjeta,codalerta)=false then

				insert into alerta (nrotarjeta,fecha,nrorechazo,codalerta,descripcion) values (c1.nrotarjeta,current_timestamp,null,codalerta,'Se realizaron dos compras en menos de un minuto');


			elsif codalerta=5 and diferencia_tiempo <= '00:05:00'::interval and c1.nrocomercio!=c2.nrocomercio  and codpostalcompra1!=codpostalcompra2 and alerta_duplicada(c1.nrotarjeta,codalerta)=false then

				insert into alerta (nrotarjeta,fecha,nrorechazo,codalerta,descripcion) values (c1.nrotarjeta,current_timestamp,null,codalerta,'Se realizaron dos compras en diferentes comercios en menos de cinco minutos');
				

			end if;

		end loop;

	end loop;

end;
$$ language plpgsql;

create or replace function alerta_duplicada(nro_tarjeta char(16), cod_alerta int) returns boolean as $$
declare
	alerta_encontrada record;
begin
	select * into alerta_encontrada from alerta where nrotarjeta=nro_tarjeta and codalerta=cod_alerta;
	
	if not found then
		return false;
	end if;

	return true;
end;
$$ language plpgsql;

create or replace function verificar_rechazo(nro_rechazo int, nro_tarjeta char(16), nro_comercio int, fecha timestamp, motivo text) returns void as $$
begin

	perform alerta_limite_compra(nro_rechazo,nro_tarjeta, fecha, motivo);

	insert into alerta (nrotarjeta,fecha,nrorechazo,codalerta,descripcion) values (nro_tarjeta,fecha,nro_rechazo,0,motivo);

end;
$$ language plpgsql;


create or replace function funcion_alerta_rechazos() returns trigger as $$
begin
	perform verificar_rechazo(new.nrorechazo, new.nrotarjeta, new.nrocomercio, new.fecha, new.motivo);

	return new;
end;
$$ language plpgsql;

create or replace function funcion_alerta_compras_min() returns trigger as $$
begin
	perform alerta_compra_minutos(1); --Verifica las compras con un tiempo menor a 1min

	perform alerta_compra_minutos(5); --Verifica las compras con un tiempo menor a 5min

	return new;
end;
$$ language plpgsql;

create trigger autorizar_trigger
before insert on consumo
for each row
execute procedure funcion_autorizar();

create trigger comprobar_rechazos
after insert on rechazo
for each row
execute procedure funcion_alerta_rechazos();

create trigger alertas_compras
after insert on compra
for each row
execute procedure funcion_alerta_compras_min();

`)

	if err != nil {
		log.Fatal(err)
	}

}

func main() {

	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjetas sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	var opcion int

	var siempre = true

	for siempre {

		fmt.Print("Por favor ingrese que opcion desea realizar\n")
		fmt.Print("1 -- Crear Base de Datos \n")
		fmt.Print("2 -- Crear Tablas en la Base de Datos\n")
		fmt.Print("3 -- Asignar Pks y Fks\n")
		fmt.Print("4 -- Eliminar Pks y Fks\n")
		fmt.Print("5 -- Cargar tablas\n")
		fmt.Print("6 -- Crear Stored Procedures y Triggers\n")
		fmt.Print("7 -- Cargar cierres\n")
		fmt.Print("8 -- Generar consumos\n")
		fmt.Print("9 -- Generar resumen\n")
		fmt.Print("0 -- Salir\n")

		fmt.Scanf("%d", &opcion)

		if opcion == 1 {
			crearDataBase()
			fmt.Printf("La base de datos se creo exitosamente\n\n")
		}

		if opcion == 2 {
			crearTablas()
			fmt.Printf("Las tablas se crearon exitosamente\n\n")
		}

		if opcion == 3 {
			asignarPKyFK()
			fmt.Printf("Las PKs y Fks de las tablas fueron creadas exitosamente\n\n")
		}

		if opcion == 4 {
			_, err = db.Exec("select borrarPKsyFKs();")

			if err != nil {
				log.Fatal(err)
			}

			fmt.Printf("Se eliminarion las Pks y Fks correctamente")

		}

		if opcion == 5 {
			cargarTablas()
			fmt.Printf("Los datos se cargaron exitosamente\n\n")
		}

		if opcion == 6 {
			crearStoredYTriggers()
			fmt.Printf("Los stored procedures y Triggers se crearon exitosamente\n\n")
		}

		if opcion == 7 {
			_, err = db.Exec("select cargar_cierres();")
			if err != nil {
				log.Fatal(err)
			}

			fmt.Printf("Se cargaron los cierres correctamente")
		}

		if opcion == 8 {
			_, err = db.Exec("select cargar_consumos();")
			if err != nil {
				log.Fatal(err)
			}

			fmt.Printf("Los consumos fueron correctamente generados")
		}

		if opcion == 9 {
			_, err = db.Exec("select funcion_resumen(1,11);")
			if err != nil {
				log.Fatal(err)
			}

			fmt.Printf("Los resumenes del cliente 1 en el mes 11 fueron generados correctamente")
		}

		if opcion == 0 {
			siempre = false
			fmt.Printf("El proceso termino exitosamente\n")
		}

	}

}
