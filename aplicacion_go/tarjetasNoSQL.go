package main

import (
	"encoding/json"
	bolt "go.etcd.io/bbolt"
	"log"
	"strconv"
)

type Cliente struct {
	NroCliente int
	Nombre     string
	Apellido   string
	Domicilio  string
	Telefono   string
}

type Tarjeta struct {
	NroTarjeta   string
	NroCliente   int
	ValidaDesde  string
	ValidaHasta  string
	CodSeguridad string
	LimiteCompra float64
	Estado       string
}

type Comercio struct {
	NroComercio  int
	Nombre       string
	Domicilio    string
	CodigoPostal string
	Telefono     string
}

type Compra struct {
	NroOperacion int
	NroTarjeta   string
	NroComercio  int
	Fecha        string
	Monto        float64
	Padago       bool
}

func CreateUpdate(db *bolt.DB, bucketName string, key []byte, val []byte) error {

	tx, err := db.Begin(true)
	if err != nil {
		return err
	}

	defer tx.Rollback()

	b, _ := tx.CreateBucketIfNotExists([]byte(bucketName))

	err = b.Put(key, val)
	if err != nil {
		return err
	}

	if err := tx.Commit(); err != nil {
		return err
	}

	return nil

}

func ReadUnique(db *bolt.DB, bucketName string, key []byte) ([]byte, error) {

	var buf []byte

	err := db.View(func(tx *bolt.Tx) error {

		b := tx.Bucket([]byte(bucketName))
		buf = b.Get(key)
		return nil
	})

	return buf, err
}

func main() {

	db, err := bolt.Open("tarjetasNoSQL.db", 0600, nil)
	if err != nil {

		log.Fatal(err)

	}

	defer db.Close()

	tarjeta := Tarjeta{"5686885897586512", 1, "201811", "202303", "4989", 30000, "vigente"}
	data1, err := json.Marshal(tarjeta)
	if err != nil {
		log.Fatalf("JSON marshaling fallo: %s", err)
	}

	tarjeta2 := Tarjeta{"6482167412032846", 2, "201605", "202501", "4678", 150000, "suspendida"}
	data2, err := json.Marshal(tarjeta2)
	if err != nil {
		log.Fatalf("JSON marshaling fallo: %s", err)
	}

	tarjeta3 := Tarjeta{"1231984838975686", 3, "201510", "202303", "1687", 970000, "anulada"}
	data3, err := json.Marshal(tarjeta3)
	if err != nil {
		log.Fatalf("JSON marshaling fallo: %s", err)
	}

	CreateUpdate(db, "Tarjeta", []byte(tarjeta.NroTarjeta), data1)
	CreateUpdate(db, "Tarjeta", []byte(tarjeta2.NroTarjeta), data2)
	CreateUpdate(db, "Tarjeta", []byte(tarjeta3.NroTarjeta), data3)

	cliente := Cliente{1, "Ricardo", "Rojas", "Paso de los Andes 2194", "1125789715"}
	data4, err := json.Marshal(cliente)
	if err != nil {
		log.Fatalf("JSON marshaling fallo: %s", err)
	}

	cliente2 := Cliente{2, "Rocio", "Paredes", "Soler 2975", "1157946828"}
	data5, err := json.Marshal(cliente2)
	if err != nil {
		log.Fatalf("JSON marshaling fallo: %s", err)
	}

	cliente3 := Cliente{3, "Luciano", "Monzon", "El Salvador 5794", "1168756849"}
	data6, err := json.Marshal(cliente3)
	if err != nil {
		log.Fatalf("JSON marshaling fallo: %s", err)
	}

	CreateUpdate(db, "Cliente", []byte(strconv.Itoa(cliente.NroCliente)), data4)
	CreateUpdate(db, "Cliente", []byte(strconv.Itoa(cliente2.NroCliente)), data5)
	CreateUpdate(db, "Cliente", []byte(strconv.Itoa(cliente3.NroCliente)), data6)

	comercio := Comercio{1, "La central", "Avenida Peron 5498", "B2701XAA", "02320-486879"}
	data7, err := json.Marshal(comercio)
	if err != nil {
		log.Fatalf("JSON marshaling fallo: %s", err)
	}
	comercio2 := Comercio{2, "Carrefour", "Remedios de Escalada 2234", "B2701XAB", "02320-484913"}
	data8, err := json.Marshal(comercio2)
	if err != nil {
		log.Fatalf("JSON marshaling fallo: %s", err)
	}
	comercio3 := Comercio{3, "Vital", "Jose de San Martin 666", "B2701XAD", "02320487777"}
	data9, err := json.Marshal(comercio3)
	if err != nil {
		log.Fatalf("JSON marshaling fallo: %s", err)
	}

	CreateUpdate(db, "Comercio", []byte(strconv.Itoa(comercio.NroComercio)), data7)
	CreateUpdate(db, "Comercio", []byte(strconv.Itoa(comercio2.NroComercio)), data8)
	CreateUpdate(db, "Comercio", []byte(strconv.Itoa(comercio3.NroComercio)), data9)

	compra := Compra{1, tarjeta.NroTarjeta, comercio.NroComercio, "2021-08-25", 17000, false}
	data10, err := json.Marshal(compra)
	if err != nil {
		log.Fatalf("JSON marshaling fallo: %s", err)
	}
	compra2 := Compra{2, tarjeta2.NroTarjeta, comercio2.NroComercio, "2020-03-17", 5000, false}
	data11, err := json.Marshal(compra2)
	if err != nil {
		log.Fatalf("JSON marshaling fallo: %s", err)
	}
	compra3 := Compra{3, tarjeta3.NroTarjeta, comercio3.NroComercio, "2021-01-08", 1056, false}
	data12, err := json.Marshal(compra3)
	if err != nil {
		log.Fatalf("JSON marshaling fallo: %s", err)
	}

	CreateUpdate(db, "Compra", []byte(strconv.Itoa(compra.NroOperacion)), data10)
	CreateUpdate(db, "Compra", []byte(strconv.Itoa(compra2.NroOperacion)), data11)
	CreateUpdate(db, "Compra", []byte(strconv.Itoa(compra3.NroOperacion)), data12)
}
